>>> 
#GULP BOILERPLATE
>>>

Just another gulp boilerplate
Migration to gulp 4 will be when im done with this one.

for those that don't know why im doing 
"gulp":"gulp" and "concurrently":"concurrently" in package.json, this is because then you dont need to install them globaly

## TODO
1. [ ] Javascript
  1. [ ] Minification
  2. [ ] Uglify
  3. [ ] AMD modules (requirejs?) 
  4. [ ] Placeholder
  5. [ ] Babel / Typescript? maybe both?
2. [ ] CSS
  1. [ ] Minification
  2. [ ] Critical inline
  3. [ ] Browser agnostic pathing for added libraries
3. [ ] HTML
  1. [ ] HTML 5 boilerplate
  2. [ ] Minification
  3. [ ] Template support?
4. [ ] Components
  1. Component support
5. [ ] Images - pictures are the evil of the web 
  1. [x] optimize - fine tuning always needed
  2. [ ] Aspect ratio scaling
  3. [ ] Image swap or progressive loading?
6. [ ] Video ? MAYBE
7. [ ] Development
  1. [x] BrowserSync