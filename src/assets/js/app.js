var app = app || {};

/* should find a namespace pattern to follow */

app.namespace = function () {

}

// Example 
app.socialMedia = function () {
  
  var facebook = function () {
    console.log('facebook')
  }

  var twitter = function () {
    console.log('twitter')
  }
  
  var instagram = function() {
    console.log('instagram')
  }
}

app.socialMedia.facebook
app.socialMedia.twitter
app.socialMedia.instagram