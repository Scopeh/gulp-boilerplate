/**
* Gulp configuration file following the 1 task 1 job
* clean tasks and lots of configuration instead
*/


/*
* Gulp related
*/
const gulp          = require('gulp')
const path          = require('path')

/*
* development 
*/
const browserSync   = require('browser-sync')
const bsReload      = browserSync.reload


/*
* Javascript
*/
const concat        = require('gulp-concat')
/*
* Css
*/
const sassy         = require('gulp-sass')

/*
* Html optimizations
*/

/*
* Images
* added plguins so they can easly be changed
*/
const imageMin      = require('gulp-imagemin')
const imageOptiPng  = require('imagemin-optipng')
const imageOptiJpg  = require('imagemin-jpegoptim')
const imageOptiGif  = require('imagemin-gifsicle')
const imageOptiSvg  = require('imagemin-svgo')

// Common 
const rename        = require('gulp-rename')

/*
* Some shortcuts
*/
var paths = {
  src:        path.join(__dirname, '/src/'),
  dist: {
    js:       path.join(__dirname, '/dist/js/'),
    css:      path.join(__dirname, '/dist/css/'),
    images:   path.join(__dirname, '/dist/images/'),
    component:path.join(__dirname, '/dist/components/')
  },
  
  js:         path.join(__dirname, '/src/assets/js/'),
  css:        path.join(__dirname, '/src/assets/css/'),
  images:     path.join(__dirname, '/src/assets/images/'),
  component:  path.join(__dirname, '/src/assets/components/'),
  
  libs:       path.join(__dirname, '/node_modules/')
}

var files = {
  html:{
    entry:paths.src + 'index.html'
  },
  js:{
    libs:[
      paths.libs + 'jquery/dist/jquery.min.js',
      paths.libs + 'bootstrap/dist/js/bootstrap.min.js',
    ],
    app:[
      paths.js + '*.js',
      paths.js + '**/*.js'
    ]
  },
  css:[
    paths.css + '*.css',
    paths.css + '**/*.css'
  ],
  component:[
    paths.component + '**/*.js',
    paths.component + '**/*.css',
    paths.component + '**/*.html',
    paths.component + '**/**/.js',
    paths.component + '**/**/.css',
    paths.component + '**/**/.html',
  ],
  scss:[
    
  ],
  images:{
    jpg:[
      paths.images + '**/*.jpg',
      paths.images + '**/*.jpeg'
    ],
    png:[
      paths.images + '**/*.png',
    ],
    gif:[
      paths.images + '**/*.gif',
    ],
    svg:[
      paths.images + '**/*.svg'
    ]
  }
}

/*
* html tasks 
* Not done!!!
*/
gulp.task('html', function () {
  return gulp.src(files.html.entry)
  .pipe()
  .pipe(gulp.dist.html)
})


gulp.task('js:component', function () {
  return gulp.src(files.components)
})
/*
* Css tasks
*/
gulp.task('css:build', function () {
  return gulp.src(files.css)
  .pipe(concat('concat.css', {newLine:';'}))
  .pipe(rename('styles-libs.css'))
  .pipe(gulp.dest(paths.dist.css))
  .pipe(browserSync.reload({stream:true}))
})

/*
* Javascript tasks
*/

gulp.task('js:app', function () {
  return gulp.src(files.js.app)
  .pipe(rename('main.js'))
  .pipe(uglifyjs())
  .pipe(gulp.dest(paths.dist.js))
  .pipe(browserSync.reload({stream:true}))
})

/*
* Image tasks
*/
gulp.task('image:png', function () {
  return gulp.src(files.images.png)
  .pipe(imageMin(
    imageOptiPng({
      optimizationLevel:5
    })
  ))
  .pipe(gulp.dest(paths.dist.images))
})

gulp.task('image:jpg', function () {
  return gulp.src(files.images.jpg)
  .pipe(imageMin(
    imageOptiJpg({
      progressive:true
    })
  ))
  .pipe(gulp.dest(paths.dist.images))
})

gulp.task('image:gif', function () {
  return gulp.src(files.images.gif)
  .pipe(imageMin(
    imageOptiGif({
      interlaced:true,
      optimizationLevel: 2,
    })
  ))
  .pipe(gulp.dest(paths.dist.images))
})

/*
* Se more settings at : https://github.com/svg/svgo
* alot can be done with SVG's and most of it is useless for web 
*/

gulp.task('image:svg', function () {
  return gulp.src(files.images.svg)
  .pipe(imageMin(
    imageOptiSvg({
      cleanupAttrs:true,
      removeXMLProcInst:true,
      removeComments:true,
      removeMetadata:true,
      removeTitle:true,
      removeDesc:true,
      removeEmptyAttrs:true,
      removeHiddenElems:true,
      removeEmptyText:true,
      removeEmptyContainers:true,
      removeViewBox:true,
      convertStyleToAttrs:true,
      convertColors:true,
      convertPathData:true,
      convertTransform:true,
      removeUnknownsAndDefaults:true,
      removeNonInheritableGroupAttrs:true,
      removeUselessStrokeAndFill:true,
      removeUnusedNS:true,
      cleanupIDs:true,
      cleanupNumericValues:true,
      cleanupListOfValues:true,
      moveElemsAttrsToGroup:true,
      moveGroupAttrsToElems:true,
      collapseGroups:true,
      removeRasterImages:true,
      mergePaths:true,
      convertShapeToPath:true,
      sortAttrs:false,
      removeDimensions:true,
      removeAttrs:false,
      removeElementsByAttr:false,
      addClassesToSVGElement:false,
      addAttributesToSVGElement:false,
      removeStyleElement:false,
      removeScriptElement:true,
    })
  ))
  .pipe(gulp.dest(paths.dist.images))
})

/* This is where the magic for development happens */

gulp.task('browserSync:create', function () {
  browserSync.create()
})
gulp.task('browserSync:dev', ['browserSync:create'], function () {
  browserSync.init({
    server: {
      baseDir: 'src'
    }
  })
})

/* Should be ok to start using in development now  
* need to clean up images so that they can do 1 image pr watch
*/

gulp.task('dev', ['browserSync:dev'], function () {
  gulp.watch(files.html.entry).on('change', bsReload)
  gulp.watch(files.js.app).on('change', bsReload)
  gulp.watch(files.css).on('change', bsReload)
  
})